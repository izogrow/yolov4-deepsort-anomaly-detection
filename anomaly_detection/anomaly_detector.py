

class AnomalyDetector:
    def __init__(self):
        self.overlap_groups =[]

    def detect_anomalies(self, tracks, min_iou=0.7, min_objects=3, min_track_age=10):
        self.detect_anomalies_by_time(min_track_age)
        self.detect_anomalies_by_overlap(tracks, min_iou, min_objects)

    def detect_anomalies_by_time(self, tracks, min_track_age=10):
        anomaly_tracks_indexes = []
        track_i = 0
        for track in tracks:
            if track.age > min_track_age:
                anomaly_tracks_indexes.append(track_i)
            track_i += 1
        return anomaly_tracks_indexes

    def detect_anomalies_by_overlap(self, tracks, min_iou=0.7, min_objects=3):
        bbox_i = 0
        bboxes = [track.to_tlbr() for track in tracks]
        # находим площади каждого прямоугольника
        s_of_bboxes = [(bbox[3] - bbox[1]) * (bbox[2] - bbox[0]) for bbox in bboxes]

        for bbox in bboxes:
            for i in range(bbox_i+1, len(bboxes)):
                overlap = self._find_overlap(bbox, bboxes[i])
                if overlap:
                    iou = overlap / (s_of_bboxes[bbox_i] + s_of_bboxes[i] - overlap)
                    if iou > min_iou:
                        self._add_to_groups([bbox_i, i])
            bbox_i += 1

        anomalies_groups = [group for group in self.overlap_groups if len(group) >= min_objects]
        return anomalies_groups

    def _find_overlap(self, bbox1, bbox2):
        dx = min(bbox1[2], bbox2[2]) - max(bbox1[0], bbox2[0])
        dy = min(bbox1[3], bbox2[3]) - max(bbox1[1], bbox2[1])
        if (dx >= 0) and (dy >= 0):
            return dx * dy

    def _add_to_groups(self, pair):
        group_i = 0
        for group in self.overlap_groups:
            for element in pair:
                if element in group:
                    self.overlap_groups[group_i].update(pair)
                    return
            group_i += 1

        self.overlap_groups.append(set(pair))


if __name__ == '__main__':
    # test for overlap finding procedure
    bbox1 = [1, 1, 8, 8]
    bbox2 = [2, 2, 6, 6]
    s_bbox1 = (bbox1[3] - bbox1[1]) * (bbox1[2] - bbox1[0])
    s_bbox2 = (bbox2[3] - bbox2[1]) * (bbox2[2] - bbox2[0])
    ad = AnomalyDetector()
    overlap = ad._find_overlap(bbox2, bbox1)
    iou = overlap / (s_bbox1 + s_bbox2 - overlap)
    print(overlap, iou)
